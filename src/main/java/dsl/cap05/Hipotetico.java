package dsl.cap05;

/**
 * Created by leonardootto on 18/12/14.
 */
public class Hipotetico implements Clause {
    private Clause clause;

    public Hipotetico(Clause clause) {
        this.clause = clause;
    }

    public String getText() {
        return clause.getText()+" hipotético";
    }
}
