package dsl.cap05;

/**
 * Created by leonardootto on 18/12/14.
 */
public class Dsl {
    public static String hipotetico(Clause clause){
        return new Hipotetico(clause).getText();
    }
    public static Exemplo exemplo(Clause clause){
        return new Exemplo(clause);
    }
    public static Um um(){
        return new Um();
    }
}
