package dsl.cap05;

import static dsl.cap05.Dsl.*;


public class Main {
    public static void main(String[] args) {
        String text = hipotetico(exemplo(um()));
        String required = "Um exemplo hipotético";
        System.out.println("Iguais? " + required.equals(text));
    }
}