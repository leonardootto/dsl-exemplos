package dsl.cap05;

/**
 * Created by leonardootto on 18/12/14.
 */
public class Exemplo implements Clause {
    private Clause clause;

    public Exemplo(Clause clause) {
        this.clause = clause;
    }

    @Override
    public String getText() {
        return clause.getText()+ " exemplo";
    }
}
