package dsl.cap05.annotation;

import java.lang.reflect.Field;

public class Handler {

    public boolean isValid(Object obj) {
        for (Field f : obj.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            PropertyAge column = f.getAnnotation(PropertyAge.class);
            if(column != null) {
                try {
                    Integer age = (Integer) f.get(obj);
                    return age >= 18;
                } catch (Exception e) {
                    return false;
                }
            }
        }
        return false;
    }

}
