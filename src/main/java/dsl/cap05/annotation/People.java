package dsl.cap05.annotation;

public class People {
    @PropertyAge
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
