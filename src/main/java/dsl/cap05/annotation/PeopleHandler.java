package dsl.cap05.annotation;

public class PeopleHandler {

    public boolean isValid(People people) {
        return people.getAge() >= 18;
    }

}
