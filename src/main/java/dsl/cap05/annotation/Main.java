package dsl.cap05.annotation;

/**
 * Created by leonardootto on 14/10/15.
 */
public class Main {
    public static void main(String[] args) {
        People p = new People();
        p.setAge(17);
        System.out.println(new Handler().isValid(p));

        p = new People();
        p.setAge(18);
        System.out.println(new Handler().isValid(p));
    }
}
