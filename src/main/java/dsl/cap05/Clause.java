package dsl.cap05;

/**
 * Created by leonardootto on 18/12/14.
 */
public interface Clause {

    public String getText();
}
