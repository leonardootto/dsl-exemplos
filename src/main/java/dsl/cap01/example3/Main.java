package dsl.cap01.example3;

import dsl.cap01.example1.Game;
import dsl.cap01.example1.Player;
import dsl.cap01.example1.Team;
import dsl.cap01.example1.event.*;

import static dsl.cap01.example3.Dsl.*;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Main {

    public static void main(String[] args) {
        Game game = directModel();
        Game game2 = dslModel();
        System.out.println("Iguais? " + game.equals(game2));
    }

    private static Game directModel() {
        Team languageTeam = new Team("Language Tools");
        Team doItTeam = new Team("Do it Team");

        //Define languageTeam Players
        // C Creator
        Player ritchie = new Player("Dennis Ritchie");
        // Java
        Player gosling = new Player("James Gosling");
        // Python
        Player guido = new Player("Guido van Rossum");
        // LISP
        Player mcCarthy = new Player("John McCarthy");
        // Ruby
        Player matsumoto = new Player("Yukihiro Matsumoto");
        // Swift
        Player lattner = new Player("Chris Lattner");
        // Perl
        Player wall = new Player("Larry Wall");
        // Javascript
        Player eich = new Player("Brendan Eich");
        // SmallTalk
        Player kan = new Player("Alan kan");
        // Scala
        Player odersky = new Player("Martin Odersky");
        // Fortran
        Player backus = new Player("John Backus");
        // C++ creator;
        Player bjarne = new Player("Bjarne Stroustrup");

        //Define doItTeam Players
        // Tdd creator
        Player beck = new Player("Kent Beck");
        // Many Many Books
        Player fowler = new Player("Martin Fowler");
        // Stackoverflow
        Player atwood = new Player("Jeff Atwood");
        // Stackoverflow
        Player spolsky = new Player("Joel Spolsky");
        // LISP Hacker
        Player graham = new Player("Paul Graham");
        // Unix co-creator
        Player thompson = new Player("Ken Thompson");
        // Unix co-creator
        Player kernighan = new Player("Brian Kernighan");
        // Clean X
        Player martin = new Player("Robert C. Martin");
        // The art of Unix programming
        Player raymond = new Player("Eric C. Raymond");
        // GNU creator
        Player stallman = new Player("Richard Stallman");
        // Linux and Git creator
        Player linus = new Player("Linus Torvalds");

        //Wire Players
        languageTeam.addPlayer(ritchie);
        languageTeam.addPlayer(gosling);
        languageTeam.addPlayer(guido);
        languageTeam.addPlayer(mcCarthy);
        languageTeam.addPlayer(matsumoto);
        languageTeam.addPlayer(lattner);
        languageTeam.addPlayer(wall);
        languageTeam.addPlayer(eich);
        languageTeam.addPlayer(kan);
        languageTeam.addPlayer(odersky);
        languageTeam.addPlayer(backus);
        //Wire Reserves
        languageTeam.addReserves(bjarne);

        //Wire Players
        doItTeam.addPlayer(beck);
        doItTeam.addPlayer(fowler);
        doItTeam.addPlayer(atwood);
        doItTeam.addPlayer(spolsky);
        doItTeam.addPlayer(graham);
        doItTeam.addPlayer(thompson);
        doItTeam.addPlayer(kernighan);
        doItTeam.addPlayer(martin);
        doItTeam.addPlayer(raymond);
        doItTeam.addPlayer(stallman);
        doItTeam.addPlayer(linus);

        // what happened in the game ?
        Game game = new Game("06/07/2014", languageTeam, doItTeam);
        game.addEvent(new StartMatch("14:00"));
        game.addEvent(new Gol(odersky, "14:33"));
        game.addEvent(new YellowCard("14:35", backus));
        game.addEvent(new IntervalStart("14:47"));
        game.addEvent(new IntervalEnd("15:00"));
        game.addEvent(new Gol(linus, "15:05"));
        game.addEvent(new YellowCard("15:15", stallman));
        game.addEvent(new Substitution("15:25", ritchie, bjarne));
        game.addEvent(new EndMatch("15:48"));

        return game;
    }

    private static Game dslModel() {
        return game()
                .schedule("06/07/2014")
                .teamOne("Language Tools",
                        player("Dennis Ritchie"),
                        player("James Gosling"),
                        player("Guido van Rossum"),
                        player("John McCarthy"),
                        player("Yukihiro Matsumoto"),
                        player("Chris Lattner"),
                        player("Larry Wall"),
                        player("Brendan Eich"),
                        player("Alan kan"),
                        player("Martin Odersky"),
                        player("John Backus"),
                        reserve("Bjarne Stroustrup")
                ).teamTwo("Do it Team",
                        player("Kent Beck"),
                        player("Martin Fowler"),
                        player("Jeff Atwood"),
                        player("Joel Spolsky"),
                        player("Paul Graham"),
                        player("Ken Thompson"),
                        player("Brian Kernighan"),
                        player("Robert C. Martin"),
                        player("Eric C. Raymond"),
                        player("Richard Stallman"),
                        player("Linus Torvalds")
                ).events(
                        start("14:00"),
                        gol("14:33", from("Martin Odersky")),
                        yellowCard("14:35").to("John Backus"),
                        intervalStart("14:47"),
                        intervalEnd("15:00"),
                        gol("15:05", from("Linus Torvalds")),
                        yellowCard("15:15").to("Richard Stallman"),
                        substitution("15:25", out("Dennis Ritchie"), in("Bjarne stroustrup")),
                        end("15:48")
                ).build();
    }
}
