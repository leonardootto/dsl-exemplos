package dsl.cap01.example3;

import dsl.cap01.example1.Event;
import dsl.cap01.example1.Game;
import dsl.cap01.example1.Player;
import dsl.cap01.example1.Team;
import dsl.cap01.example1.event.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by leonardootto on 09/07/2014.
 */
public class Dsl {

    private String date;

    private List<Event> events = new ArrayList<Event>();
    private Team teamOne;
    private Team teamTwo;

    public static Dsl game() {
        return new Dsl();
    }

    public Game build() {
        //return game
        Game game = new Game(date, teamOne, teamTwo);
        game.setEvents(events);
        return game;
    }

    public static PlayerDecorator player(String name) {
        return new TitularPlayer(new Player(name));
    }

    public static PlayerDecorator reserve(String name) {
        return new HolderPlayer(new Player(name));
    }

    public interface PlayerDecorator {
        void addInTeam(Team team);
    }

    static class HolderPlayer implements PlayerDecorator {
        private Player player;

        public HolderPlayer(Player player) {
            this.player = player;
        }

        @Override
        public void addInTeam(Team team) {
            team.addReserves(player);
        }
    }

    static class TitularPlayer implements PlayerDecorator {
        private Player player;

        public TitularPlayer(Player player) {
            this.player = player;
        }

        @Override
        public void addInTeam(Team team) {
            team.addPlayer(player);
        }

    }

    public Dsl schedule(String date) {
        this.date = date;
        return this;
    }

    public Dsl teamOne(String teamName, PlayerDecorator... players) {
        this.teamOne = new Team(teamName);
        List<PlayerDecorator> list = Arrays.asList(players);
        for (PlayerDecorator pd : list) {
            pd.addInTeam(teamOne);
        }
        return this;
    }

    public Dsl teamTwo(String teamName, PlayerDecorator... players) {
        this.teamTwo = new Team(teamName);
        List<PlayerDecorator> list = Arrays.asList(players);
        for (PlayerDecorator pd : list) {
            pd.addInTeam(teamTwo);
        }
        return this;
    }

    public Dsl events(Event... events) {
        this.events.addAll(Arrays.asList(events));
        return this;
    }

    public static StartMatch start(String time) {
        return new StartMatch(time);
    }

    public static Gol gol(String time, Player player) {
        return new Gol(player, time);
    }

    public static EndMatch end(String time) {
        return new EndMatch(time);
    }

    public static IntervalStart intervalStart(String time) {
        return new IntervalStart(time);
    }

    public static IntervalEnd intervalEnd(String time) {
        return new IntervalEnd(time);
    }

    public static DYellowEvent yellowCard(String time) {
        return new DYellowEvent(time);
    }

    public static Player from(String playerName) {
        return new Player(playerName);
    }
    public static Player out(String playerName) {
        return new Player(playerName);
    }

    public static Player in(String playerName) {
        return new Player(playerName);
    }

    public static Substitution substitution(String time, Player out, Player in) {
        return new Substitution(time, out, in);
    }

    public static class DYellowEvent {
        private String time;

        public DYellowEvent(String time) {
            this.time = time;
        }

        public Event to(String playerName) {
            return new YellowCard(this.time, new Player(playerName));
        }
    }
}



