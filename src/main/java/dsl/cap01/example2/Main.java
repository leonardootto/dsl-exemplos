package dsl.cap01.example2;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;
import com.thoughtworks.xstream.converters.extended.ToAttributedValueConverter;
import dsl.cap01.example1.Event;
import dsl.cap01.example1.Game;
import dsl.cap01.example1.Player;
import dsl.cap01.example1.Team;
import dsl.cap01.example1.event.*;

import java.util.TimeZone;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Main {

    public static void main(String[] args) {
        try {
            Game game = createGame();
            Game game2 = loadFromXMLFile("game.xml");
            System.out.println("Iguais? " + game.equals(game2));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Game createGame() {
        Team languageTeam = new Team("Language Tools");
        Team doItTeam = new Team("Do it Team");

        //Define languageTeam Players
        // C Creator
        Player ritchie = new Player("Dennis Ritchie");
        // Java
        Player gosling = new Player("James Gosling");
        // Python
        Player guido = new Player("Guido van Rossum");
        // LISP
        Player mcCarthy = new Player("John McCarthy");
        // Ruby
        Player matsumoto = new Player("Yukihiro Matsumoto");
        // Swift
        Player lattner = new Player("Chris Lattner");
        // Perl
        Player wall = new Player("Larry Wall");
        // Javascript
        Player eich = new Player("Brendan Eich");
        // SmallTalk
        Player kan = new Player("Alan kan");
        // Scala
        Player odersky = new Player("Martin Odersky");
        // Fortran
        Player backus = new Player("John Backus");
        // C++ creator;
        Player bjarne = new Player("Bjarne Stroustrup");

        //Define doItTeam Players
        // Tdd creator
        Player beck = new Player("Kent Beck");
        // Many Many Books
        Player fowler = new Player("Martin Fowler");
        // Stackoverflow
        Player atwood = new Player("Jeff Atwood");
        // Stackoverflow
        Player spolsky = new Player("Joel Spolsky");
        // LISP Hacker
        Player graham = new Player("Paul Graham");
        // Unix co-creator
        Player thompson = new Player("Ken Thompson");
        // Unix co-creator
        Player kernighan = new Player("Brian Kernighan");
        // Clean X
        Player martin = new Player("Robert C. Martin");
        // The art of Unix programming
        Player raymond = new Player("Eric C. Raymond");
        // GNU creator
        Player stallman = new Player("Richard Stallman");
        // Linux and Git creator
        Player linus = new Player("Linus Torvalds");

        //Wire Players
        languageTeam.addPlayer(ritchie);
        languageTeam.addPlayer(gosling);
        languageTeam.addPlayer(guido);
        languageTeam.addPlayer(mcCarthy);
        languageTeam.addPlayer(matsumoto);
        languageTeam.addPlayer(lattner);
        languageTeam.addPlayer(wall);
        languageTeam.addPlayer(eich);
        languageTeam.addPlayer(kan);
        languageTeam.addPlayer(odersky);
        languageTeam.addPlayer(backus);
        //Wire Reserves
        languageTeam.addReserves(bjarne);

        //Wire Players
        doItTeam.addPlayer(beck);
        doItTeam.addPlayer(fowler);
        doItTeam.addPlayer(atwood);
        doItTeam.addPlayer(spolsky);
        doItTeam.addPlayer(graham);
        doItTeam.addPlayer(thompson);
        doItTeam.addPlayer(kernighan);
        doItTeam.addPlayer(martin);
        doItTeam.addPlayer(raymond);
        doItTeam.addPlayer(stallman);
        doItTeam.addPlayer(linus);

        // what happened in the game ?
        Game game = new Game("06/07/2014", languageTeam, doItTeam);
        game.addEvent(new StartMatch("14:00"));
        game.addEvent(new Gol(odersky, "14:33"));
        game.addEvent(new YellowCard("14:35", backus));
        game.addEvent(new IntervalStart("14:47"));
        game.addEvent(new IntervalEnd("15:00"));
        game.addEvent(new Gol(linus, "15:05"));
        game.addEvent(new YellowCard("15:15", stallman));
        game.addEvent(new Substitution("15:25", ritchie, bjarne));
        game.addEvent(new EndMatch("15:48"));

        return game;
    }

    private static String convertToXml(Game game) {
        return getConfiguredXStream().toXML(game);
    }

    private static Game loadFromXMLFile(String xml) throws Exception {
        XStream xStream = getConfiguredXStream();
        Object obj = xStream.fromXML(Main.class.getResourceAsStream("/game.xml"));
        return (Game) obj;
    }

    private static XStream getConfiguredXStream() {
        XStream xStream = new XStream();
        xStream.setMode(XStream.NO_REFERENCES);
        xStream.alias("game", Game.class);
        DateConverter converter = new DateConverter("dd/MM/yyyy", new String[]{}, TimeZone.getTimeZone("UTC"));
        xStream.registerLocalConverter(Game.class, "date", converter);
        xStream.registerLocalConverter(Event.class, "time", new DateConverter("HH:mm", new String[]{}));
        xStream.alias("player", Player.class);
        xStream.registerConverter(new ToAttributedValueConverter(Player.class, xStream.getMapper(), xStream.getReflectionProvider(), xStream.getConverterLookup(), "name"));
        xStream.alias("startMatch", StartMatch.class);
        xStream.alias("gol", Gol.class);
        xStream.alias("yellowCard", YellowCard.class);
        xStream.alias("intervalStart", IntervalStart.class);
        xStream.alias("intervalEnd", IntervalEnd.class);
        xStream.alias("substitution", Substitution.class);
        xStream.alias("endMatch", EndMatch.class);
        Class[] classes = {
                StartMatch.class, IntervalEnd.class,
                IntervalStart.class, EndMatch.class
        };
        registerConverterForAll(xStream, classes, "time", Event.class);
        return xStream;
    }

    private static void registerConverterForAll(XStream xStream, Class[] classes, String atributeName, Class<Event> atributeClass) {
        for (Class c : classes) {
            xStream.registerConverter(new ToAttributedValueConverter(c, xStream.getMapper(), xStream.getReflectionProvider(), xStream.getConverterLookup(), atributeName, atributeClass));
        }
    }
}
