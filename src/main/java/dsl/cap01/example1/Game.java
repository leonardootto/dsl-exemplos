package dsl.cap01.example1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Game {
    private Date date;
    private Team teamOne;
    private Team teamTwo;
    private static SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    private List<Event> events = new ArrayList<Event>();

    public Game(String gameDate, Team one, Team two){
        date = parseDate(gameDate);
        teamOne = one;
        teamTwo = two;
    }

    private Date parseDate(String gameDate) {
        try {
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            return format.parse(gameDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Date format "+gameDate+" is Invalid use dd/MM/yyyy format!");
        }
    }

    public void addEvent(Event event){
        events.add(event);
    }

    public Team getTeamOne() {
        return teamOne;
    }

    public Team getTeamTwo() {
        return teamTwo;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (!date.equals(game.date)) return false;
        if (!events.equals(game.events)) return false;
        if (!teamOne.equals(game.teamOne)) return false;
        if (!teamTwo.equals(game.teamTwo)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + teamOne.hashCode();
        result = 31 * result + teamTwo.hashCode();
        result = 31 * result + events.hashCode();
        return result;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }
}
