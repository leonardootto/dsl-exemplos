package dsl.cap01.example1;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Player {
    private String name;

    public Player(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (!name.equals(player.name))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
