package dsl.cap01.example1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by leonardootto on 06/07/2014.
 */
public abstract class Event {
    private Date time;
    private static SimpleDateFormat format = new SimpleDateFormat("HH:mm");

    public Event(String strTime){
        time = parseDate(strTime);
    }

    private Date parseDate(String strTime) {
        try {
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            return format.parse(strTime);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Date format "+strTime+" is Invalid HH:mm format!");
        }
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Event event = (Event) o;

        if (!time.equals(event.time))
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return time.hashCode();
    }
}
