package dsl.cap01.example1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Team {
    private String name;
    private List<Player> players = new ArrayList<Player>();
    private List<Player> reserves = new ArrayList<Player>();

    public void addPlayer(Player player) {
        this.players.add(player);
    }

    public void addReserves(Player player) {
        this.reserves.add(player);
    }

    public Team(String name) {
        this.name = name;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Player> getReserves() {
        return reserves;
    }

    public void setReserves(List<Player> reserves) {
        this.reserves = reserves;
    }

    /**
     * Considerei que um time com jogadores sem jogadores é igual uma lista vazia
     * O xStream é chato para deixar tags de coleções vazias para poder diferenciar
     * entre as duas coisa.
     *
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        if (!name.equals(team.name)) return false;
        if (!equalsPlayersWhenNullSameAsEmpty(players, team.players)) return false;
        if (!equalsPlayersWhenNullSameAsEmpty(reserves, team.reserves)) return false;

        return true;
    }

    private boolean equalsPlayersWhenNullSameAsEmpty(List<Player> players, List<Player> otherReserves) {
        boolean leftEmpty = players == null || players.isEmpty();
        boolean rightEmpty = otherReserves == null || otherReserves.isEmpty();

        if (leftEmpty && rightEmpty) return true;
        if (leftEmpty ^ rightEmpty) return false;
        if (!(players.equals(otherReserves))) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + players.hashCode();
        result = 31 * result + (reserves != null ? reserves.hashCode() : 0);
        return result;
    }
}
