package dsl.cap01.example1.event;

import dsl.cap01.example1.Player;

/**
 * Created by leonardootto on 06/07/2014.
 */
public class YellowCard extends CardEvent {

    public YellowCard(String inTime, Player player) {
        super(inTime,player);
    }
}
