package dsl.cap01.example1.event;

import dsl.cap01.example1.Event;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class StartMatch extends Event {

    public StartMatch(String startTime) {
        super(startTime);
    }

}
