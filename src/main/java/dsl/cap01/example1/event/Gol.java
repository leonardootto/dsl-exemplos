package dsl.cap01.example1.event;

import dsl.cap01.example1.Event;
import dsl.cap01.example1.Player;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Gol extends Event {
    private Player player;

    public Player getPlayer() {
        return player;
    }

    public Gol(Player player,String inTime) {
        super(inTime);
        this.player = player;
    }
}
