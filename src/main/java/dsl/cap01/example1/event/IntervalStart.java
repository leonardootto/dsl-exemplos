package dsl.cap01.example1.event;

import dsl.cap01.example1.Event;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class IntervalStart extends Event {
    public IntervalStart(String inTime) {
        super(inTime);
    }
}
