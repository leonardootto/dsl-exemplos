package dsl.cap01.example1.event;

import dsl.cap01.example1.Event;
import dsl.cap01.example1.Player;

/**
 * Created by leonardootto on 05/07/2014.
 */
public abstract class CardEvent extends Event {
    private Player player;

    protected CardEvent(String inTime, Player player) {
        super(inTime);
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }
}
