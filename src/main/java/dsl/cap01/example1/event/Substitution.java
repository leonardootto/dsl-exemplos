package dsl.cap01.example1.event;

import dsl.cap01.example1.Event;
import dsl.cap01.example1.Player;

/**
 * Created by leonardootto on 05/07/2014.
 */
public class Substitution extends Event {
    private Player in;
    private Player out;

    public Player getIn() {
        return in;
    }

    public Player getOut() {
        return out;
    }

    public Substitution(String inTime, Player in, Player out) {
        super(inTime);
        this.in = in;
        this.out = out;
    }
}
