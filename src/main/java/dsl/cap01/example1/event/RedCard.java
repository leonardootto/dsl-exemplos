package dsl.cap01.example1.event;

import dsl.cap01.example1.Player;

/**
 * Created by leonardootto on 06/07/2014.
 */
public class RedCard extends CardEvent {
    public RedCard(String inTime, Player player) {
        super(inTime, player);
    }
}
