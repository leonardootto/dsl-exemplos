package dsl.cap02.example4;

import dsl.cap02.example1.model.Group;
import dsl.cap02.example1.model.Shortcut;
import dsl.cap02.example1.model.shortcut.CharKey;
import dsl.cap02.example1.model.shortcut.ModifierKey;

import static dsl.cap02.example1.model.shortcut.ModifierKey.*;

/**
 * Created by leonardootto on 05/09/2014.
 */
class DShortcut{
    private Shortcut model;

    protected DShortcut(Shortcut model){
        this.model = model;
    }

    public Shortcut getModel() {
        return model;
    }
    public DShortcut ctrl() {
        model.addKey(Ctrl);
        return this;
    }

    public DShortcut shift() {
        model.addKey(Shift);
        return this;
    }

    public DShortcut alt() {
        model.addKey(Alt);
        return this;
    }

    public DShortcut plus(char key) {
        model.addKey(new CharKey(key));
        return this;
    }

    public DShortcut del() {
        model.addKey(Del);
        return this;
    }
    public DShortcut f1() {
        model.addKey(F1);
        return this;
    }

    public DShortcut f2() {
        model.addKey(F2);
        return this;
    }

    public DShortcut f3() {
        model.addKey(F3);
        return this;
    }

    public DShortcut f4() {
        model.addKey(F4);
        return this;
    }

    public DShortcut f5() {
        model.addKey(F5);
        return this;
    }

    public DShortcut f6() {
        model.addKey(F6);
        return this;
    }

    public DShortcut f7() {
        model.addKey(F7);
        return this;
    }

    public DShortcut f8() {
        model.addKey(F8);
        return this;
    }

    public DShortcut f9() {
        model.addKey(F9);
        return this;
    }

    public DShortcut f10() {
        model.addKey(F10);
        return this;
    }

    public DShortcut f11() {
        model.addKey(F11);
        return this;
    }

    public DShortcut f12() {
        model.addKey(F12);
        return this;
    }

}


public class DSL {

    private DSL group;

    public static Group group(DShortcut... shortcut) {
        Group group = new Group();
        for (DShortcut s : shortcut) {
            group.addShortcut(s.getModel());
        }
        return group;
    }

    public static DShortcut shortcut() {
        return new DShortcut(new Shortcut());
    }

    public boolean addShortcut(Shortcut shortcut) {
        return group.addShortcut(shortcut);
    }


}
