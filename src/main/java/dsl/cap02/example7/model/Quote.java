package dsl.cap02.example7.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by leonardootto on 09/09/2014.
 */
public class Quote {
    private String exchange;
    private String name;
    private List<String> symbols = new ArrayList<String>();
    private String industry;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addSymbol(String symbol) {
        this.symbols.add(symbol);
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }


    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        Quote quote = (Quote) o;

        if(exchange != null ? !exchange.equals(quote.exchange) : quote.exchange != null)
            return false;
        if(name != null ? !name.equals(quote.name) : quote.name != null) return false;
        if(symbols != null ? !symbols.equals(quote.symbols) : quote.symbols != null) return false;
        return industry != null ? industry.equals(quote.industry) : quote.industry == null;

    }

    @Override
    public int hashCode() {
        int result = exchange != null ? exchange.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (symbols != null ? symbols.hashCode() : 0);
        result = 31 * result + (industry != null ? industry.hashCode() : 0);
        return result;
    }
}