package dsl.cap02.example7;

import dsl.cap02.example7.model.Quote;

/**
 * Created by leonardootto on 09/09/2014.
 */
public class Main {
    public static void main(String[] args) {
        Quote quoteFromDsl = quoteFromDsl();
        Quote quoteFromCQ = quoteFromCommandQuery();

        System.out.println("Iguais? " + quoteFromCQ.equals(quoteFromDsl));
    }

    private static Quote quoteFromCommandQuery() {
        Quote quote = new Quote();
        quote.setExchange("NASDAQ");
        quote.setName("Google Inc");
        quote.addSymbol("GOOG");
        quote.addSymbol("GOOGL");
        quote.setIndustry("Technology");
        return quote;
    }

    private static Quote quoteFromDsl() {
        return Builder.build(quote -> {
            quote.exchange = "NASDAQ";
            quote.name = "Google Inc";
            quote.symbols(s -> {
                s.add("GOOG");
                s.add("GOOGL");
            });
            quote.industry = "Technology";
        });
    }
}
