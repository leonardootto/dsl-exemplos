package dsl.cap02.example7;

import dsl.cap02.example7.model.Quote;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by leonardootto on 09/09/2014.
 */
public class Builder {
    public String exchange;
    public String name;
    public String industry;
    private List<String> symbols = new ArrayList<>();

    private Builder() {
    }

    public static Quote build(Consumer<Builder> consumer) {
        Builder builder = new Builder();
        consumer.accept(builder);
        return builder.build();
    }

    public void symbols(Consumer<List<String>> consumer) {
        consumer.accept(symbols);
    }

    private Quote build() {
        Quote quote = new Quote();
        quote.setExchange(exchange);
        quote.setName(name);
        quote.setIndustry(industry);
        for (String symbol : symbols) {
            quote.addSymbol(symbol);
        }
        return quote;
    }
}
