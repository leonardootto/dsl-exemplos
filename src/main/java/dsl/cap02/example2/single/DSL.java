package dsl.cap02.example2.single;

import dsl.cap02.example1.model.Group;
import dsl.cap02.example1.model.Shortcut;

import java.util.ArrayList;

/**
 * Created by leonardootto on 01/09/2014.
 */
public class DSL {
    private static Group actualGroup = new Group();

    private DSL() {
    }

    public static void group() {
        actualGroup = new Group();
    }

    private static Group actualGroup() {
        if (actualGroup == null) {
            throw new IllegalArgumentException("group() not called");
        }
        return actualGroup;
    }

    public static void shortcut() {
        actualGroup().addShortcut(new Shortcut());
    }

    public static void ctrl() {
        actualGroup().lastShortcut().ctrl();
    }

    public static void shift() {
        actualGroup().lastShortcut().shift();
    }

    public static void alt() {
        actualGroup().lastShortcut().alt();
    }

    public static void plus(char key) {
        actualGroup().lastShortcut().plus(key);
    }

    public static void del() {
        actualGroup().lastShortcut().del();
    }

    public static void f1() {
        actualGroup().lastShortcut().f1();
    }

    public static void f2() {
        actualGroup().lastShortcut().f2();
    }

    public static void f3() {
        actualGroup().lastShortcut().f3();
    }

    public static void f4() {
        actualGroup().lastShortcut().f4();
    }

    public static void f5() {
        actualGroup().lastShortcut().f5();
    }

    public static void f6() {
        actualGroup().lastShortcut().f6();
    }

    public static void f7() {
        actualGroup().lastShortcut().f7();
    }

    public static void f8() {
        actualGroup().lastShortcut().f8();
    }

    public static void f9() {
        actualGroup().lastShortcut().f9();
    }

    public static void f10() {
        actualGroup().lastShortcut().f10();
    }

    public static void f11() {
        actualGroup().lastShortcut().f11();
    }

    public static void f12() {
        actualGroup().lastShortcut().f12();
    }

    public static Group end() {
        Group group = actualGroup();
        actualGroup = null;
        return group;
    }
}
