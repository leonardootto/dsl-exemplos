package dsl.cap02.example3;

import dsl.cap02.example1.model.Group;
import dsl.cap02.example1.model.Shortcut;
import dsl.cap02.example1.model.shortcut.CharKey;

import static dsl.cap02.example1.model.shortcut.ModifierKey.*;
import static dsl.cap02.example3.DSL.*;

/**
 * Created by leonardootto on 01/09/2014.
 */
public class Main {
    public static void main(String[] args) {
        Group groupFromDsl = getGroupFromDsl();
        Group groupFromCQ = getGroupFromCommandQuery();

        System.out.println("Iguais? " + groupFromCQ.equals(groupFromDsl));
    }

    private static Group getGroupFromDsl() {
        return group(
                shortcut(
                        ctrl(),
                        plus('z')
                ),
                shortcut(
                        ctrl(),
                        shift(),
                        plus('z')
                ),
                shortcut(
                        ctrl(),
                        alt(),
                        del()
                ),
                shortcut(
                        alt(),
                        f4()
                )
        );
    }


    private static Group getGroupFromCommandQuery() {
        Group g = new Group();
        Shortcut s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(new CharKey('z'));
        g.addShortcut(s);

        s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(Shift);
        s.addKey(new CharKey('z'));
        g.addShortcut(s);

        s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(Alt);
        s.addKey(Del);
        g.addShortcut(s);

        s = new Shortcut();
        s.addKey(Alt);
        s.addKey(F4);
        g.addShortcut(s);

        return g;
    }

}
