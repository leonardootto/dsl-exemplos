package dsl.cap02.example3;

import dsl.cap02.example1.model.Group;
import dsl.cap02.example1.model.Shortcut;
import dsl.cap02.example1.model.shortcut.CharKey;
import dsl.cap02.example1.model.shortcut.Key;
import dsl.cap02.example1.model.shortcut.ModifierKey;

import static dsl.cap02.example1.model.shortcut.ModifierKey.*;

/**
 * Created by leonardootto on 01/09/2014.
 */
public class DSL {

    public static Group group(Shortcut... shortcut) {
        Group group = new Group();
        for (Shortcut s : shortcut) {
            group.addShortcut(s);
        }
        return group;
    }

    public static Shortcut shortcut(Key... keys) {
        Shortcut shortcut = new Shortcut();
        for (Key key : keys) {
            shortcut.addKey(key);
        }
        return shortcut;
    }

    public static Key ctrl() {
        return Ctrl;
    }

    public static Key alt() {
        return Alt;
    }

    public static Key shift() {
        return Shift;
    }

    public static Key del() {
        return Del;
    }

    public static Key plus(char c) {
        return new CharKey(c);
    }

   public static Key f1() {
        return F1;
    }

   public static Key f2() {
        return F2;
    }

   public static Key f3() {
        return F3;
    }

   public static Key f4() {
        return F4;
    }

   public static Key f5() {
        return F5;
    }

   public static Key f6() {
        return F6;
    }

   public static Key f7() {
        return F7;
    }

   public static Key f8() {
        return F8;
    }

   public static Key f9() {
        return F9;
    }

   public static Key f10() {
        return F10;
    }

   public static Key f11() {
        return F11;
    }

   public static Key f12() {
        return F12;
    }


}



