package dsl.cap03.example1.direct;

import dsl.cap03.example1.direct.model.Group;
import dsl.cap03.example1.direct.model.Shortcut;
import dsl.cap03.example1.direct.model.shortcut.CharKey;

import static dsl.cap03.example1.direct.model.Group.*;
import static dsl.cap03.example1.direct.model.shortcut.ModifierKey.*;

/**
 * Created by leonardootto on 31/08/2014.
 */
public class Main {
    public static void main(String[] args) {
        Group groupFromDsl = getGroupFromDsl();
        Group groupFromCQ = getGroupFromCommandQuery();

        System.out.println("Iguais? " + groupFromCQ.equals(groupFromDsl));
    }

    private static Group getGroupFromCommandQuery() {
        Group g = new Group();
        Shortcut s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(new CharKey('z'));
        g.addShortcut(s);

        s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(Shift);
        s.addKey(new CharKey('z'));
        g.addShortcut(s);

        s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(Alt);
        s.addKey(Del);
        g.addShortcut(s);

        s = new Shortcut();
        s.addKey(Alt);
        s.addKey(F4);
        g.addShortcut(s);

        return g;
    }

    private static Group getGroupFromDsl() {
        return group()
                .shortcut().ctrl().plus('z')
                .shortcut().ctrl().shift().plus('z')
                .shortcut().ctrl().alt().del()
                .shortcut().alt().f4();

    }

    private static void test(){
        new Group().addShortcut(new Shortcut().ctrl().plus('z'));

    }
}
