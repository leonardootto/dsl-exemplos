package dsl.cap03.example1.direct.model.shortcut;

/**
 * Created by leonardootto on 01/09/2014.
 */
public enum ModifierKey implements Key {
    Alt, Ctrl, Del, Shift,
    F1, F2, F3, F4, F5, F6,
    F7, F8, F9, F10, F11, F12
}
