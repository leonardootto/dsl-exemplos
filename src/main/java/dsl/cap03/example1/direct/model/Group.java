package dsl.cap03.example1.direct.model;

import java.util.ArrayList;

/**
 * Created by leonardootto on 02/09/2014.
 */
public class Group {
    private ArrayList<Shortcut> list = new ArrayList<>();

    public Group() {
    }

    public boolean addShortcut(Shortcut shortcut) {
        return this.list.add(shortcut);
    }

    public Shortcut lastShortcut() {
        if (list.size() == 0) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (list != null ? !list.equals(group.list) : group.list != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }

    /* Dsl methods */
    public static Group group() {
        return new Group();
    }

    public Group shortcut() {
        Shortcut shortcut = new Shortcut();
        addShortcut(shortcut);
        return this;
    }

    public Group ctrl() {
        lastShortcut().ctrl();
        return this;
    }

    public Group plus(char key) {
        lastShortcut().plus(key);
        return this;
    }

    public Group shift() {
        lastShortcut().shift();
        return this;
    }

    public Group alt() {
        lastShortcut().alt();
        return this;
    }


    public Group del() {
        lastShortcut().del();
        return this;
    }

    public Group f1() {
        lastShortcut().f1();
        return this;
    }

    public Group f2() {
        lastShortcut().f2();
        return this;
    }

    public Group f3() {
        lastShortcut().f3();
        return this;
    }

    public Group f4() {
        lastShortcut().f4();
        return this;
    }

    public Group f5() {
        lastShortcut().f5();
        return this;
    }

    public Group f6() {
        lastShortcut().f6();
        return this;
    }

    public Group f7() {
        lastShortcut().f7();
        return this;
    }

    public Group f8() {
        lastShortcut().f8();
        return this;
    }

    public Group f9() {
        lastShortcut().f9();
        return this;
    }

    public Group f10() {
        lastShortcut().f10();
        return this;
    }

    public Group f11() {
        lastShortcut().f11();
        return this;
    }

    public Group f12() {
        lastShortcut().f12();
        return this;
    }


}
