package dsl.cap03.example1.direct.model.shortcut;

/**
 * Created by leonardootto on 01/09/2014.
 */
public class CharKey implements Key {
    public final char key;

    public CharKey(char key){
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharKey charKey = (CharKey) o;

        if (key != charKey.key) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) key;
    }

}
