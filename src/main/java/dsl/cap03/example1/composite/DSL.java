package dsl.cap03.example1.composite;

import dsl.cap03.example1.composite.model.Group;
import dsl.cap03.example1.composite.model.Shortcut;

/**
 * Created by leonardootto on 29/08/2014.
 */
public class DSL {

    private final Group group;

    private DSL() {
        this.group = new Group();
    }

    public static DSL group() {
        return new DSL();
    }

    public DSL shortcut() {
        Shortcut shortcut = new Shortcut();
        addShortcut(shortcut);
        return this;
    }

    public boolean addShortcut(Shortcut shortcut) {
        return group.addShortcut(shortcut);
    }

    public DSL ctrl() {
        lastShortcut().ctrl();
        return this;
    }

    public DSL plus(char key) {
        lastShortcut().plus(key);
        return this;
    }

    private Shortcut lastShortcut() {
        return group.lastShortcut();
    }

    public DSL shift() {
        lastShortcut().shift();
        return this;
    }

    public DSL alt() {
        lastShortcut().alt();
        return this;
    }

    public DSL del() {
        lastShortcut().del();
        return this;
    }

    public DSL f1() {
        lastShortcut().f1();
        return this;
    }

    public DSL f2() {
        lastShortcut().f2();
        return this;
    }

    public DSL f3() {
        lastShortcut().f3();
        return this;
    }

    public DSL f4() {
        lastShortcut().f4();
        return this;
    }

    public DSL f5() {
        lastShortcut().f5();
        return this;
    }

    public DSL f6() {
        lastShortcut().f6();
        return this;
    }

    public DSL f7() {
        lastShortcut().f7();
        return this;
    }

    public DSL f8() {
        lastShortcut().f8();
        return this;
    }

    public DSL f9() {
        lastShortcut().f9();
        return this;
    }

    public DSL f10() {
        lastShortcut().f10();
        return this;
    }

    public DSL f11() {
        lastShortcut().f11();
        return this;
    }

    public DSL f12() {
        lastShortcut().f12();
        return this;
    }

    public Group build() {
        return group;
    }
}
