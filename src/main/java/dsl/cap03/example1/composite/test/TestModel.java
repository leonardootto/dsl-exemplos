package dsl.cap03.example1.composite.test;

import dsl.cap03.example1.composite.model.Group;
import dsl.cap03.example1.composite.model.Shortcut;
import dsl.cap03.example1.composite.model.shortcut.CharKey;
import org.junit.Test;

import static dsl.cap03.example1.composite.model.shortcut.ModifierKey.Ctrl;

/**
 * Created by leonardootto on 09/04/16.
 */
public class TestModel {

    @Test(expected = IllegalStateException.class)
    public void testSpecialRepetition() {
        Group g = new Group();
        Shortcut s = new Shortcut();
        s.addKey(Ctrl);
        s.addKey(Ctrl);
        g.addShortcut(s);
    }

    @Test(expected = IllegalStateException.class)
    public void testCharKeyRepetition() {
        Group g = new Group();
        Shortcut s = new Shortcut();
        s.addKey(new CharKey('k'));
        s.addKey(new CharKey('k'));
        s.addKey(new CharKey('k'));
        g.addShortcut(s);
    }
}
