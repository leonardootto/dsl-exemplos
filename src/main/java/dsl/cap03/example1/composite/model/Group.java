package dsl.cap03.example1.composite.model;

import java.util.ArrayList;

/**
 * Created by leonardootto on 02/09/2014.
 */
public class Group {
    private ArrayList<Shortcut> list = new ArrayList<>();

    public Group() {
    }

    public boolean addShortcut(Shortcut shortcut) {
        return this.list.add(shortcut);
    }

    public Shortcut lastShortcut() {
        if (list.size() == 0) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (list != null ? !list.equals(group.list) : group.list != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }
}
