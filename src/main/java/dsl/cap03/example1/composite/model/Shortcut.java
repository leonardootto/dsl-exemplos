package dsl.cap03.example1.composite.model;

import dsl.cap03.example1.composite.model.shortcut.CharKey;
import dsl.cap03.example1.composite.model.shortcut.Key;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static dsl.cap03.example1.composite.model.shortcut.ModifierKey.*;

/**
 * Created by leonardootto on 29/08/2014.
 */
public class Shortcut {

    private Set<Key> keys = new HashSet<Key>();

    public Shortcut addKey(Key key) {
        boolean add = keys.add(key);
        if(!add){
            throw new IllegalStateException("Duplicate key:"+key);
        }
        return this;
    }

    public Shortcut ctrl() {
        return addKey(Ctrl);
    }
    public Shortcut plus(char key) {
        return addKey(new CharKey(key));
    }

    public Shortcut shift() {
        return addKey(Shift);
    }

    public Shortcut alt() {
        return addKey(Alt);

    }

    public Shortcut f1() {
        return addKey(F1);

    }

    public Shortcut f2() {
        return addKey(F2);

    }

    public Shortcut f3() {
        return addKey(F3);

    }

    public Shortcut f4() {
        return addKey(F4);

    }

    public Shortcut f5() {
        return addKey(F5);

    }

    public Shortcut f6() {
        return addKey(F6);

    }

    public Shortcut f7() {
        return addKey(F7);

    }

    public Shortcut f8() {
        return addKey(F8);

    }

    public Shortcut f9() {
        return addKey(F9);

    }

    public Shortcut f10() {
        return addKey(F10);

    }

    public Shortcut f11() {
        return addKey(F11);

    }

    public Shortcut f12() {
        return addKey(F12);

    }

    public Shortcut del() {
        return addKey(Del);

    }

    public Set<Key> getKeys() {
        return keys;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shortcut shortcut = (Shortcut) o;
        if (keys != null ? !keys.equals(shortcut.keys) : shortcut.keys != null) return false;
        return true;
    }
    @Override
    public int hashCode() {
        return keys != null ? keys.hashCode() : 0;
    }
}
