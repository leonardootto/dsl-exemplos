package dsl.cap01.example4

import _root_.dsl.cap01.example1.event._
import _root_.dsl.cap01.example1.{Event, Game, Player, Team}

/**
 * Created by leonardootto on 25/08/2014.
 *
 */
object Dsl {

  object game {
    def apply(date: String, team1: Team, team2: Team, event: Seq[Event]) = {
      val game: Game = new Game(date, team1, team2)
      event.foreach(game.addEvent)
      game
    }
  }

  object team {
    def apply(name: String)(players: Players) = {
      val team: Team = new Team(name)
      players.listPlayers.foreach(team.addPlayer)
      players.listReserves.foreach(team.addReserves)
      team
    }
  }

  implicit def stringToPlayer(name: String) = new Player(name)

  class Players(val listPlayers: Seq[Player]) {
    var listReserves: Seq[Player] = Seq()

    def reserves(players: Player*): Players = {
      this.listReserves = players
      this
    }
  }

  def players(players: Player*): Players = {
    new Players(players)
  }

  ////
  def events(events: DEvent*): Seq[Event] = {
    events.map {
      _.toDomainEvent
    }
  }


  trait DEvent {
    def toDomainEvent: Event = {
      this match {
        case start: DStartEvent => new StartMatch(start.time)
        case end: DEndEvent => new EndMatch(end.time)
        case event: DYellowCardEvent => new YellowCard(event.time, event.player)
        case event: DIntervalEvent => new IntervalStart(event.time)
        case event: DIntervalEnd => new IntervalEnd(event.time)
        case event: DSubstitutionEvent => new Substitution(event.time, event.inPlayer, event.outPlayer)
        case event: DGolEvent => new Gol(event.player, event.time)

        case _ => throw new IllegalStateException("Event not mapped!")
      }
    }
  }

  def gol = new DGolEvent()

  class DGolEvent extends DEvent {
    var time = ""
    var player: Player = null

    def from(player: String): DGolEvent = {
      this.player = player
      this
    }

    def at(time: String): DGolEvent = {
      this.time = time
      this
    }
  }

  def start = new DStartEvent()

  class DStartEvent extends DEvent {
    var time = ""

    def at(time: String): DStartEvent = {
      this.time = time
      this
    }
  }

  def end = new DEndEvent()

  class DEndEvent extends DEvent {
    var time: String = ""

    def at(time: String): DEndEvent = {
      this.time = time
      this
    }
  }

  //yellowCard at "15:05" to "John Backus"
  def yellowCard = new DYellowCardEvent()

  class DYellowCardEvent extends DEvent {
    var time = ""
    var player: Player = null


    def at(time: String): DYellowCardEvent = {
      this.time = time
      this
    }

    def to(player: Player): DYellowCardEvent = {
      this.player = player
      this
    }
  }

  //intervalStart at "14:47"
  def intervalStart = new DIntervalEvent()

  class DIntervalEvent extends DEvent {
    var time = ""

    def at(time: String): DIntervalEvent = {
      this.time = time
      this
    }
  }

  // intervalEnd at "15:00"
  def intervalEnd = new DIntervalEnd()

  class DIntervalEnd extends DEvent {
    var time = ""

    def at(time: String): DIntervalEnd = {
      this.time = time
      this
    }
  }

  //          substitution at "15:25" out "Dennis Ritchie" in "Bjarne stroustrup"
  def substitution = new DSubstitutionEvent()

  class DSubstitutionEvent extends DEvent {
    var inPlayer: Player = null
    var outPlayer: Player = null
    var time: String = ""

    def at(time: String): DSubstitutionEvent = {
      this.time = time
      this
    }

    def out(player: Player): DSubstitutionEvent = {
      this.outPlayer = player
      this
    }

    def in(player: Player): DSubstitutionEvent = {
      this.inPlayer = player
      this
    }
  }

  //          end at "15:27"

}
