package dsl.cap01.example4

import _root_.dsl.cap01.example1.event._
import _root_.dsl.cap01.example1.{Game, Player, Team}
import Dsl._


object Main {

  def main(args: Array[String]) {
      val dslGame = getFromDslApi
      val cqGame = getFromCommandQueryApi

      println("Iguais? " + (dslGame equals cqGame))
  }

  def getFromDslApi: Game ={
    game("06/07/2014",
      team("Language Tools") {
        players(
          "Dennis Ritchie",
          "James Gosling",
          "Guido van Rossum",
          "John McCarthy",
          "Yukihiro Matsumoto",
          "Chris Lattner",
          "Larry Wall",
          "Brendan Eich",
          "Alan kan",
          "Martin Odersky",
          "John Backus"
        ) reserves (
          "Bjarne Stroustrup"
        )
      },
      team("Do it Team") {
        players(
          "Kent Beck",
          "Martin Fowler",
          "Jeff Atwood",
          "Joel Spolsky",
          "Paul Graham",
          "Ken Thompson",
          "Brian Kernighan",
          "Robert C. Martin",
          "Eric C. Raymond",
          "Richard Stallman",
          "Linus Torvalds"
        )
      },
      events(
        start at "14:00",
        gol from "John Backus" at "14:33",
        yellowCard at "14:35" to "John Backus",
        intervalStart at "14:47",
        intervalEnd at "15:00",
        gol from "Linus Torvalds" at "15:05",
        yellowCard at "15:15" to "Richard Stallman",
        substitution at "15:25" out "Dennis Ritchie" in "Bjarne stroustrup",
        end at "15:48"
      )
    )
  }

  def getFromCommandQueryApi: Game = {
    val languageTeam: Team = new Team("Language Tools")
    val doItTeam: Team = new Team("Do it Team")

    //Define languageTeam Players
    // C Creator
    val ritchie: Player = new Player("Dennis Ritchie")
    // Java
    val gosling: Player = new Player("James Gosling")
    // Python
    val guido: Player = new Player("Guido van Rossum")
    // LISP
    val mcCarthy: Player = new Player("John McCarthy")
    // Ruby
    val matsumoto: Player = new Player("Yukihiro Matsumoto")
    // Swift
    val lattner: Player = new Player("Chris Lattner")
    // Perl
    val wall: Player = new Player("Larry Wall")
    // Javascript
    val eich: Player = new Player("Brendan Eich")
    // SmallTalk
    val kan: Player = new Player("Alan kan")
    // Scala
    val odersky: Player = new Player("Martin Odersky")
    // Fortran
    val backus: Player = new Player("John Backus")
    // C++ creator;
    val bjarne: Player = new Player("Bjarne Stroustrup")

    //Define doItTeam Players
    // Tdd creator
    val beck: Player = new Player("Kent Beck")
    // Many Many Books
    val fowler: Player = new Player("Martin Fowler")
    // Stackoverflow
    val atwood: Player = new Player("Jeff Atwood")
    // Stackoverflow
    val spolsky: Player = new Player("Joel Spolsky")
    // LISP Hacker
    val graham: Player = new Player("Paul Graham")
    // Unix co-creator
    val thompson: Player = new Player("Ken Thompson")
    // Unix co-creator
    val kernighan: Player = new Player("Brian Kernighan")
    // Clean X
    val martin: Player = new Player("Robert C. Martin")
    // The art of Unix programming
    val raymond: Player = new Player("Eric C. Raymond")
    // GNU creator
    val stallman: Player = new Player("Richard Stallman")
    // Linux and Git creator
    val linus: Player = new Player("Linus Torvalds")

    //Wire Players
    languageTeam.addPlayer(ritchie)
    languageTeam.addPlayer(gosling)
    languageTeam.addPlayer(guido)
    languageTeam.addPlayer(mcCarthy)
    languageTeam.addPlayer(matsumoto)
    languageTeam.addPlayer(lattner)
    languageTeam.addPlayer(wall)
    languageTeam.addPlayer(eich)
    languageTeam.addPlayer(kan)
    languageTeam.addPlayer(odersky)
    languageTeam.addPlayer(backus)
    //Wire Reserves
    languageTeam.addReserves(bjarne)

    //Wire Players
    doItTeam.addPlayer(beck)
    doItTeam.addPlayer(fowler)
    doItTeam.addPlayer(atwood)
    doItTeam.addPlayer(spolsky)
    doItTeam.addPlayer(graham)
    doItTeam.addPlayer(thompson)
    doItTeam.addPlayer(kernighan)
    doItTeam.addPlayer(martin)
    doItTeam.addPlayer(raymond)
    doItTeam.addPlayer(stallman)
    doItTeam.addPlayer(linus)

    // what happened in the game ?
    val game: Game = new Game("06/07/2014", languageTeam, doItTeam)

    game.addEvent(new StartMatch("14:00"))
    game.addEvent(new Gol(odersky, "14:33"))
    game.addEvent(new YellowCard("14:35", backus))
    game.addEvent(new IntervalStart("14:47"))
    game.addEvent(new IntervalEnd("15:00"))
    game.addEvent(new Gol(linus, "15:05"))
    game.addEvent(new YellowCard("15:15", stallman))
    game.addEvent(new Substitution("15:25", ritchie, bjarne))
    game.addEvent(new EndMatch("15:48"))

    game
  }



}