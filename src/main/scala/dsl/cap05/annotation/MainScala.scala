package dsl.cap05.annotation

object MainScala {

  case class People(@PropertyAge age: Int)

  class Handler {
    def isValid(obj: Any): Boolean = {
      for (f <- obj.getClass.getDeclaredFields) {
        f.setAccessible(true)
        val column = f.getAnnotation(classOf[PropertyAge])
        if (column != null) {
          try {
            val age = f.get(obj).asInstanceOf[Integer]
            return age >= 18
          } catch {
            case e: Exception => {
              return false
            }
          }
        }
      }
      false
    }
  }

  def main(args: Array[String]) {
    println(new Handler().isValid(People(17)))
  }
}
