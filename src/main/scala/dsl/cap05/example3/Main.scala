package dsl.cap05.example3

object Task{
  def apply(name: String, body: => Unit): Task = new Task(name, body)
}

class Task(val name: String, body: => Unit) {
  def call(): Unit = {
    body
  }
}

case class Dependency(task: Task, dependsOn: String)

object Main extends TaskBuilder {

  def main(args: Array[String]) {
    task("init") {
      println("init")
    }
    task("prepare", depends = "init") {
      println("prepare")
    }
    task("compile", depends = "prepare") {
      println("compile")
    }
    task("package", depends = "clean,compile") {
      println("package")
    }
    task("run", depends = "compile") {
      println("run")
    }
    task("clean", depends = "init") {
      println("clean")
    }

    run("package")
  }
}

trait TaskBuilder {

  var list = List[Dependency]()
  var alreadyRun = List[Task]()

  def task(name: String, depends: String = "")(body: => Unit): Unit = {
    list = list :+ Dependency(new Task(name, body), depends)
  }

  def run(taskName: String): Unit = {

    getTaskDependencies(taskName).foreach(task => {
      task.dependsOn.split(",").foreach(taskName => {
        run(taskName)
      })
    })
    if (getTaskByName(taskName).isDefined) {
      val task = getTaskByName(taskName).get

      if (!alreadyRun.contains(task)) {
        task.call()
        alreadyRun = alreadyRun :+ task
      }
    }
  }

  def getTaskDependencies(name: String): Option[Dependency] = {
    list.find(_.task.name == name)
  }

  def getTaskByName(name: String): Option[Task] = {
    getTaskDependencies(name).map(_.task)
  }
}



