package dsl.cap05.example2

import dsl.cap05.example2.TableValue.TableValue

/**
  * Created by leonardootto on 14/01/15.
  */

object TableValue {
  case object S extends TableValue
  case object N extends TableValue
  case object X extends TableValue

  class TableValue() {
    override def equals(any: scala.Any): Boolean = {
      if (!any.isInstanceOf[AnyRef]) return false

      val ref = any.asInstanceOf[AnyRef]
      if (this.eq(X) || ref.eq(X)) {
        true
      } else {
        this eq ref
      }
    }
  }
}

case class Row(values: List[TableValue], string: String)

case class Table
(
  rows: List[Row],
  cons: List[Row]
) {
  def result(input: List[TableValue]): List[TableValue] = {
    for (column <- 0 to rows.head.values.size) {
      val colList = getColList(column, rows)
      if (input.equals(colList)) {
        return getColList(column, cons)
      }
    }
    List()
  }

  def getColList(col: Int, list: List[Row]): List[TableValue] = {
    list.collect({
      case row: Row if col < row.values.length => row.values(col)
    })
  }
}

object Main {
  def main(args: Array[String]) {
    import TableValue._;

    val table = Table(
      List(
        Row(List(S, N, N, N, N), "Mais perto da linha da meta que penultimo"),
        Row(List(X, X, X, X, S), "Própria metade de campo"),
        Row(List(X, S, X, X, X), "Mesma linha que penúltimo"),
        Row(List(X, X, S, X, X), "Mesma linha dois ultimos"),
        Row(List(X, X, X, S, X), "Tiro de meta, Arremesso lateral ou tiro de canto")
      ),
      List(
        Row(List(S, N, N, N, N), "Impedido")
      ))

    val result = table.result(List(S, S, S, N, N))
    println(result)
  }
}



