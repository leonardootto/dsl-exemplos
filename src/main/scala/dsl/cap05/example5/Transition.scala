package dsl.cap05.example5

/**
  * Created by leonardootto on 05/07/2016.
  */
case class Transition(startState: State, func: () => (Boolean, State)) {
}