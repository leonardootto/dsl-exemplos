package dsl.cap05.example5

import dsl.cap05.example5.Sex.Sex
import dsl.cap05.example5.States._
import org.apache.commons.math3.distribution.NormalDistribution


object Sex extends Enumeration {
  type Sex = Value
  val Male, Female = Value
}

object States {
  def Live = State("Live")

  def Single = State("Single")

  def MakeOut = State("MakeOut")

  def Dating = State("Dating")

  def Fiance = State("Fiance")

  def Married = State("Married")

  def Widower = State("Widower")

  def Died = State("Died")
}

case class State(name: String)


/**
  * Created by leonardootto on 09/10/16.
  */
case class Person(age: Int,
                  name: String,
                  sex: Sex,
                  state: State) {

  def liveToSingleDistribution() = new NormalDistribution(18, 1)

  def singleDistribution() = new NormalDistribution(18, 1)

  def makeOutToDatingDistribution() = new NormalDistribution(22, 1)

  def datingToFianceDistribution() = new NormalDistribution(26, 1)

  def fianceToMarriedDistribution() = new NormalDistribution(28, 1)

  def marriedToWidowerDistribution() = new NormalDistribution(48, 1)

  def deadDistribution(sex: Sex) = {
    val maleDeadDistribution = new NormalDistribution(80, 2)
    val femaleDeadDistribution = new NormalDistribution(93, 2)
    if (sex == Sex.Male) maleDeadDistribution else femaleDeadDistribution
  }

  def machine(): Machine = {
    def transite(start: State, end: State, dist: NormalDistribution): () => (Boolean, State) = () => {
      val probability = dist.probability(age - 1, age)
      if (Math.random() < probability) {
        (true, end)
      } else {
        (false, start)
      }
    }

    Machine(
      //start state
      state,
      //all states can go to died
      Transition(Live, transite(Live, Died, deadDistribution(sex))),
      Transition(Single, transite(Single, Died, deadDistribution(sex))),
      Transition(MakeOut, transite(MakeOut, Died, deadDistribution(sex))),
      Transition(Dating, transite(Dating, Died, deadDistribution(sex))),
      Transition(Fiance, transite(Fiance, Died, deadDistribution(sex))),
      Transition(Married, transite(Married, Died, deadDistribution(sex))),
      Transition(Widower, transite(Widower, Died, deadDistribution(sex))),

      Transition(Live, transite(Live, Single, liveToSingleDistribution())),
      Transition(Single, transite(Single, MakeOut, singleDistribution())),

      Transition(MakeOut, transite(MakeOut, Single, makeOutToDatingDistribution())),
      Transition(MakeOut, transite(MakeOut, Dating, makeOutToDatingDistribution())),


      Transition(Dating, transite(Dating, Fiance, datingToFianceDistribution())),
      Transition(Dating, transite(Dating, Single, singleDistribution())),


      Transition(Fiance, transite(Fiance, Single, singleDistribution())),
      Transition(Fiance, transite(Fiance, Married, fianceToMarriedDistribution())),

      Transition(Married, transite(Married, Single, singleDistribution())),
      Transition(Married, transite(Married, Widower, marriedToWidowerDistribution())),

      Transition(Widower, transite(Widower, Single, singleDistribution())),
      Transition(Widower, transite(Widower, Died, singleDistribution()))
    )
  }

  def nextYear(people: Set[Person]): Person = {
    val tick = machine().tick()

    if (tick.state != state) {
      println(s"${name} transitou de ${state.name} para ${tick.state.name} com  ${age} ano(s)")
    }
    copy(state = tick.state)
  }
}