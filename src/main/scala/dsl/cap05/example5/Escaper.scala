package dsl.cap05.example5

import com.google.common.escape.Escapers

/**
  * Created by 314056 on 18/09/16.
  */
object Escaper {
  val escaper = Escapers.builder().addEscape('\n', "\\\\n").addEscape('\r', "\\\\r").build();

  def escape(txt: String): String = {
    escaper.escape("" + txt)
  }
}
