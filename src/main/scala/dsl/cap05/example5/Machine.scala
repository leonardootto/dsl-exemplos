package dsl.cap05.example5

/**
  * Created by leonardootto on 09/10/16.
  */

case class Machine(state: State, transitions: Transition*) {
  def tick(): Machine = {

    val allFunctions = transitions.filter(t => t.startState == state)

    allFunctions.foreach(transition => {
      val ret = transition.func()
      if (ret._1) {
        return Machine(ret._2, transitions: _*)
      }
    })
    //if not encounter any transition stay
    this
  }
}