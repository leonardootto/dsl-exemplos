package dsl.cap05.example5

import dsl.cap05.example5.{Machine, Transition}
import dsl.cap05.example5.States._

/**
  * Created by 314056 on 18/09/16.
  */
object MachineToGraphViz {

  def convert(m: Machine): String = {
    val str = m.transitions.foldRight(new StringBuilder)(
      (t, sb) => {
        sb append t.startState.name + "->" + t.func()._2.name + "\n"
      }
    )
    return str.mkString
  }

  def transite(start: State, end: State): () => (Boolean, State) = {
    return ()=>{(true,end)}
  }

  def diedTransition(): () => (Boolean, State) = {
     ()=>{(true,Died)}
  }


  def transite(endState: State): () => (Boolean, State) = {
    ()=>{(true, endState)}
  }

  def main(args: Array[String]): Unit = {
    def machine = Machine(States.Live,
      Transition(Live, transite(Live, Died)),
      Transition(Single, transite(Single, Died)),
      Transition(MakeOut, transite(MakeOut, Died)),
      Transition(Dating, transite(Dating, Died)),
      Transition(Fiance, transite(Fiance, Died)),
      Transition(Married, transite(Married, Died)),
      Transition(Widower, transite(Widower, Died)),

      Transition(Live, transite(Live, Single)),
      Transition(Single, transite(Single, MakeOut)),

      Transition(MakeOut, transite(MakeOut, Single)),
      Transition(MakeOut, transite(MakeOut, Dating)),


      Transition(Dating, transite(Dating, Fiance)),
      Transition(Dating, transite(Dating, Single)),


      Transition(Fiance, transite(Fiance, Single)),
      Transition(Fiance, transite(Fiance, Married)),

      Transition(Married, transite(Married, Single)),
      Transition(Married, transite(Married, Widower)),

      Transition(Widower, transite(Widower, Single)),
      Transition(Widower, transite(Widower, Died))
    )

    val text: String = MachineToGraphViz.convert(machine)
    println(text)

  }
}


