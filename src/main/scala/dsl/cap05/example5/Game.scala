package dsl.cap05.example5

import dsl.cap05.example5.States._
import io.codearte.jfairy.Fairy
import io.codearte.jfairy.producer.person.{Person => FPerson}


object Game {
  def peopleNumber = 10

  def apply(): Game = {
    val fairy = Fairy.create()

    val set: Set[Person] = (0 to peopleNumber).toSet.map((idx: Int) => {
      val person = fairy.person()
      val sex = if (person.sex() == FPerson.Sex.MALE) Sex.Male else Sex.Female
      val age = 0 //all people just start their's life
      Person(age, person.fullName(), sex, Live)
    })

    new Game(set)
  }
}

/**
  * Created by leonardootto on 09/10/16.
  */
case class Game(people: Set[Person]) {
  def numberLivePersons() = {
    people.count(_.state != Died)
  }

  def numberDiedPersons() = {
    people.count(_.state == Died)
  }

  def nextYear(): Game = {
    //add one year for all people
    val game = this.copy(
      people = people.map(person => {
        person.copy(age = person.age + 1)
      })
    )
    //transite each people
    game.copy(people = game.people.map(person => {
      person.nextYear(game.people)
    }))

  }
}
