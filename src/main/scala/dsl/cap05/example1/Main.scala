package dsl.cap05.example1

/**
 * Created by leonardootto on 14/01/15.
 */


object Main {
  def main(args: Array[String]) {
    case class Player(name: String, size: Double, nationality: String)

    val players = List(
      Player("Gheorghe Mureșan", 2.31, "ROU"),
      Player("Manute Bol", 2.31, "SUD"),
      Player("Shawn Bradley", 2.29, "USA"),
      Player("Yao Ming", 2.29, "CHN"),
      Player("Chuck Nevitt", 2.26, "USA"),
      Player("Pavel Podkolzin", 2.26, "RUS"),
      Player("Slavko Vraneš", 2.26, "MNE"),
      Player("Mark Eaton", 2.24, "USA"),
      Player("Mark Eaton", 2.24, "NED"),
      Player("Rik Smits", 2.24, "USA"),
      Player("Ralph Sampson", 2.24, "USA"),
      Player("Priest Lauderdale", 2.24, "USA"),
      Player("Randy Breuer", 2.21, "USA"),
      Player("Keith Closs", 2.21, "USA"),
      Player("Swede Halbrook", 2.21, "USA"),
      Player("Žydrūnas Ilgauskas", 2.21, "LTU"),
      Player("Aleksandar Radojević", 2.21, "BIH"),
      Player("Peter John Ramos", 2.21, "PUR"),
      Player("Arvydas Sabonis", 2.21, "LTU"),
      Player("Ha Seung-Jin", 2.21, "KOR"),
      Player("Hasheem Thabeet", 2.21, "TZA")
    )

    var total: Double = 0
    for (i <- 0 until players.length) {
      val player = players(i)
      total = total + player.size
    }
    val midSize = total / players.length

    //maiores
    var bigger: List[Player] = List()
    for (i <- 0 until players.length) {
      val player = players(i)
      if (player.size >= midSize) {
        bigger = bigger :+ player
      }
    }

    //dos USA
    var fromUSA: List[Player] = List()
    for (i <- 0 until players.length) {
      val player = players(i)
      if (player.nationality == "USA") {
        fromUSA = fromUSA :+ player
      }
    }

    class Filter[T](elements: List[T]) {
      def filter(filter: FilterFunction[T]): List[T] = {
        var list = List[T]()
        for (t <- elements) {
          if (filter.filter(t)) {
            list = list :+ t
          }
        }
        list
      }
    }
    trait FilterFunction[T] {
      def filter(value: T): Boolean
    }

    //maiores
    class PlayerBigSizeFilter(midSize: Double) extends FilterFunction[Player] {
      override def filter(player: Player): Boolean = {
        if (player.size >= midSize) {
          true
        } else {
          false
        }
      }
    }
    val list = new Filter(players).filter(new PlayerBigSizeFilter(midSize))

    // dos estados unidos
    class PlayerFromNationality(nationality: String) extends FilterFunction[Player] {
      override def filter(player: Player): Boolean = {
        if (player.nationality == nationality) {
          true
        } else {
          false
        }
      }
    }
    var usaPlayers = new Filter(players).filter(new PlayerFromNationality("USA"))

    //maiores jogadores
    val maiores = players.filter(p=> p.size > midSize)
    //americanos
    val americanos = players.filter(p=> p.nationality == "USA")

  }
}



