package dsl.cap02.exampleTicket

import java.util.Date

/**
 * Created by leonardootto on 22/09/15.
 */
case class Ticket(id: String, confirmation: String, date: Date, place: String, amount: Float, description: String)

class TicketBuilder {
  var id: Option[String] = None
  var confirmation: Option[String] = None
  var date: Option[Date] = None
  var place: Option[String] = None
  var amount: Option[Float] = None
  var description: Option[String] = None

  def withId(id: String) = {
    this.id = Some(id)
    this
  }

  def withConfirmation(confirmation: String) = {
    this.confirmation = Some(confirmation)
    this
  }

  def withDate(date: Date) = {
    this.date = Some(date)
    this
  }

  def withPlace(place: String) = {
    this.place = Some(place)
    this
  }

  def withAmount(amount: Float) = {
    this.amount = Some(amount)
    this
  }

  def withDescription(description: String) = {
    this.description = Some(description)
    this
  }

  def build(): Ticket = new Ticket(id.get, confirmation.get, date.get, place.get, amount.get, description.get)
}




