package dsl.cap02.exampleTicket

import java.util.Date

/**
 * Created by leonardootto on 22/09/15.
 */
class Main {
  def main(args: Array[String]) {

    val builder = new TicketBuilder()
    builder.withId("1")
      .withConfirmation("xptoconfirmation")
      .withDate(new Date())
      .withPlace("Credicard Hall")
    .withAmount(65.00f)
    .withDescription("Offspring Show")

    val ticket: Ticket = builder.build()
  }
}
