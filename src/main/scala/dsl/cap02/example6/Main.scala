package dsl.cap02.example6

import dsl.cap02.example6.model.Quote
import Dsl._
/**
 * Created by leonardootto on 01/09/2014.
 */
object Main {
  def main(args: Array[String]) {
    val quoteFromDsl: Quote = getGroupFromDsl
    val quoteFromCQ: Quote = getGroupFromCommandQuery
    System.out.println("Iguais? " + (quoteFromDsl == quoteFromCQ))
  }

  private def getGroupFromDsl: Quote = {
    return quote(
      exchange= "NASDAQ",
      name = "Google Inc",
      symbols = ('GOOG,'GOOGL),
      industry = "Technology"
    )
  }

  private def getGroupFromCommandQuery: Quote = {
    val quote = new Quote()
    quote.exchange = "NASDAQ"
    quote.name = "Google Inc"
    quote.symbols = Seq('GOOG, 'GOOGL)
    quote.industry = "Technology"
    return quote
  }
}


