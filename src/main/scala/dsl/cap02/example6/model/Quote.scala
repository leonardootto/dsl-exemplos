package dsl.cap02.example6.model

/**
 * Created by leonardootto on 09/09/2014.
 *
 */

class Quote {
  var exchange: String = ""
  var name: String = ""
  var symbols: Seq[Symbol] = Seq()
  var industry: String = ""


  def canEqual(other: Any): Boolean = other.isInstanceOf[Quote]

  override def equals(other: Any): Boolean = other match {
    case that: Quote =>
      (that canEqual this) &&
        exchange == that.exchange &&
        name == that.name &&
        symbols == that.symbols &&
        industry == that.industry
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(exchange, name, symbols, industry)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
