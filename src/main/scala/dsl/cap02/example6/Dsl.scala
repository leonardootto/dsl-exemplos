package dsl.cap02.example6

import dsl.cap02.example6.model.Quote

/**
 * Created by leonardootto on 09/09/2014.
 *
 */
object Dsl {
  def quote(exchange: String, name: String, symbols: Seq[Symbol], industry: String): Quote = {
    val quote = new Quote()
    quote.exchange = exchange
    quote.name = name
    quote.symbols = symbols
    quote.industry = industry
    quote
  }

  implicit def convertTupleToList[A <: Product](tuples: A): Seq[Symbol] = {
    val arity = tuples.productArity
    var seq: Seq[Symbol] = Seq()
    for (idx <- 0 until arity) {
      val element: Any = tuples.productElement(idx)
      element match {
        case symbol: Symbol =>
          seq = seq :+ symbol
        case _ =>
      }
    }
    seq
  }
}

