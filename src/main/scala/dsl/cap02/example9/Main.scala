package dsl.cap02.example9

import dsl.cap02.example9.Dsl._

/**
 * Created by leonardootto on 11/09/2014.
 *
 */
object Main {
  def main(args: Array[String]): Unit = {
    val equalsGrams = (1.kg + 1.kg + 500.g + 220.hectogram) == 24.5.kg
    val equalsMeters = (1.m + 50.cm + 50.decimeter) == 6.5.m

    println("Igual gramas?:" + equalsGrams)
    println("Igual metros?:" + equalsMeters)
  }
}
