package dsl.cap02.example9.model

import java.lang.Math._


/**
 * Created by leonardootto on 11/09/2014.
 *
 */
class Gram(val value: Double, val tenExpo: Long) {
  def +(number: Int) {
    new Gram(value + number, tenExpo)
  }

  def +(other: Gram): Gram = {
    if (other.tenExpo == tenExpo) {
      new Gram(value + other.value, tenExpo)
    } else {
      val otherValueConverted: Double = other.toGram / pow(10, tenExpo)
      new Gram(value + otherValueConverted, tenExpo)
    }
  }

  def toGram: Double = {
    value * pow(10, tenExpo)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[Gram]

  override def equals(other: Any): Boolean = other match {
    case that: Gram =>
      (that canEqual this) &&
        value == that.value &&
        tenExpo == that.tenExpo
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(value, tenExpo)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }


}