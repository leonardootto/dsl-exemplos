package dsl.cap02.example9.model

import java.lang.Math._

/**
 * Created by leonardootto on 11/09/2014.
 *
 */
class Meters(val value: Double, val tenExpo: Long) {
  def +(number: Int) {
    new Meters(value + number, tenExpo)
  }

  def +(other: Meters): Meters = {
    if (other.tenExpo == tenExpo) {
      new Meters(value + other.value, tenExpo)
    } else {
      val otherValueConverted: Double = other.toMeters / pow(10, tenExpo)
      new Meters(value + otherValueConverted, tenExpo)
    }
  }

  def toMeters: Double = {
    value * Math.pow(10, tenExpo)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[Meters]

  override def equals(other: Any): Boolean = other match {
    case that: Meters =>
      (that canEqual this) &&
        value == that.value &&
        tenExpo == that.tenExpo
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(value, tenExpo)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

