package dsl.cap02.example9

import dsl.cap02.example9.model.{Meters, Gram}

/**
 * Created by leonardootto on 11/09/2014.
 *
 */
object Dsl {

  implicit class IntImplicitsGrams(number: Double) {
    def kilogram: Gram = {
      new Gram(number, 3)
    }

    def kg = kilogram

    def hectogram = {
      new Gram(number, 2)
    }

    def hg = hectogram

    def dekagram = {
      new Gram(number, 1)
    }

    def dag = dekagram

    def gram = {
      new Gram(number, 0)
    }

    def g = gram

    def decigram = {
      new Gram(number, -1)
    }

    def dg = decigram

    def centigram = {
      new Gram(number, -2)
    }

    def cg = centigram

    def miligram = {
      new Gram(number, -3)
    }

    def mg = miligram
  }

  implicit class IntImplicitsMeters(number: Double) {
    def kilometer = {
      new Meters(number, 3)
    }

    def km = kilometer

    def hectometer = {
      new Meters(number, 2)
    }

    def hm = hectometer

    def dekameter = {
      new Meters(number, 1)
    }

    def dam = dekameter

    def meter = {
      new Meters(number, 0)
    }

    def m = meter

    def decimeter = {
      new Meters(number, -1)
    }

    def dm = decimeter

    def centimeter = {
      new Meters(number, -2)
    }

    def cm = centimeter

    def milimeter = {
      new Meters(number, -3)
    }

    def mm = milimeter
  }

}
