package dsl.cap02.example10

import scala.language.dynamics
import java.io.{File, FilenameFilter}

/**
 * Created by leonardootto on 11/09/2014.
 *
 */
object Ribbon {
  def apply(func: (Ribbon) => Unit): List[String] = {
    val rDef = new Ribbon
    func(rDef)
    rDef.list
  }
}

class Ribbon extends Dynamic {
  var dir: String = ""
  var ext: String = ""

  def selectDynamic(methodName: String) {
    methodName match {
      case extension if methodName.startsWith("extension") =>
        this.ext = methodName.replace("extension_", "")
      case directory if methodName.startsWith("directory") =>
        this.dir = methodName.replace("directory ", "")
    }
  }

  def directory(dir: String) = {
    this.dir = dir
  }

  def list: List[String] = {
    new File(this.dir).list(new FilenameFilter {
      override def accept(dir: File, name: String): Boolean = {
        name.endsWith(ext)
      }
    }).toList
  }
}
