package dsl.cap02.example10

/**
 * Created by leonardootto on 11/09/2014.
 *
 */
object Main {
  def main(args: Array[String]): Unit = {
    Ribbon(f => {
      f.`directory C:\\Windows\\Logs`
      f.extension_log
    }).foreach(println)
  }
}
