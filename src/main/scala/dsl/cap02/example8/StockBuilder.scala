package dsl.cap02.example8

import dsl.cap02.example8.model.Stock

/**
 * Created by leonardootto on 10/09/2014.
 *
 */
object StockBuilder {
  def apply(bilderFunction: StockBuilder => Unit):Stock = {
    val builder = new StockBuilder()
    bilderFunction(builder)
    builder.build()
  }
}
class StockBuilder {
  var exchange_ : String = ""
  var name_ : String = ""
  var symbols_ : Seq[String] = Seq()
  var industry_ : String = ""

  def exchange(exchange: String) = {
    exchange_ = exchange
  }

  def name(name: String) = {
    name_ = name
  }

  def symbols(symbols: String*) = {
    symbols_ = symbols
  }

  def industry(industry: String) = {
    industry_ = industry
  }

  def build():Stock = {
    new Stock(exchange_,name_,symbols_,industry_)
  }
}
