package dsl.cap02.example8

import dsl.cap02.example8.model.Stock


/**
 * Created by leonardootto on 10/09/2014.
 *
 */
object Main {
  def main(args: Array[String]) {
    val quoteFromDsl: Stock = getGroupFromDsl
    val quoteFromCQ: Stock = getGroupFromCommandQuery
    System.out.println("Iguais? " + (quoteFromDsl == quoteFromCQ))
  }

  private def getGroupFromDsl: Stock = {
    return StockBuilder(stock => {
      stock exchange "NASDAQ"
      stock name "Google Inc"
      stock symbols("GOOG", "GOOGL")
      stock industry "Technology"
    })
  }

  private def getGroupFromCommandQuery: Stock = {
    val stock = new Stock("NASDAQ","Google Inc",
      Seq("GOOG", "GOOGL"),"Technology")
    return stock
  }
}
