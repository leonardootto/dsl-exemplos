package dsl.cap02.example8.model

/**
 * Created by leonardootto on 26/01/15.
 */
case class Stock(exchange: String,
                 name: String,
                 symbols: Seq[String],
                 industry: String)
