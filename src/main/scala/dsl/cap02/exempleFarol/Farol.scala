package dsl.cap02.exempleFarol

import java.util.{Timer, TimerTask}

/**
 * Created by leonardootto on 08/05/15.
 */

object Farol {
  def apply(verde: => Unit,
            amarelo: => Unit,
            vermelho: => Unit) = {

    new Farol(
      ()=>verde,
      ()=>amarelo,
      ()=>vermelho)
  }
}

class Farol(verde: () => Unit,
            amarelo: () => Unit,
            vermelho: () => Unit) {

  val list = List(verde, amarelo, vermelho)
  var timer: Timer = null
  var index = 0

  def on() = {
    timer = new Timer()
    timer.scheduleAtFixedRate(new TimerTask {
      override def run(): Unit = {
        list(index)()
        index = if (index + 1 == list.size) 0 else index + 1

      }
    }, 1000, 1000)
  }

  def off() = {
    timer.cancel()
  }


}
