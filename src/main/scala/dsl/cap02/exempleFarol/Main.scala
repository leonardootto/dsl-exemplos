package dsl.cap02.exempleFarol

/**
 * Created by leonardootto on 08/05/15.
 */
object Main {
  def main(args: Array[String]) {
    Farol(
      verde = {
        println("Verde")
      },
      amarelo = {
        println("Amarelo")
      },
      vermelho = {
        println("Vermelho")
      }).on()
  }
}

