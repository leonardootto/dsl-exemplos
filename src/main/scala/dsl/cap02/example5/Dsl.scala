package dsl.cap02.example5

import dsl.cap02.example1.model.shortcut.{CharKey, Key, ModifierKey}
import dsl.cap02.example1.model.{Group, Shortcut}

/**
 * Created by leonardootto on 05/09/2014.
 *
 */
object Dsl {
  def group(shortcuts: Shortcut*): Group = {
    val group = new Group()
    shortcuts.foreach(group.addShortcut)
    group
  }

  def shortcut(keys: Seq[Key]): Shortcut = {
    val shortcut = new Shortcut
    keys.foreach(shortcut.addKey)
    shortcut
  }

  def shortcut(key: Key): Shortcut = {
    shortcut(List(key))
  }

  def symbolToKey(sym: Symbol): Key = {
    sym.name.toLowerCase match {
      case "ctrl" => ModifierKey.Ctrl
      case "alt" => ModifierKey.Alt
      case "shift" => ModifierKey.Shift
      case "del" => ModifierKey.Del
      case "f1" => ModifierKey.F1
      case "f2" => ModifierKey.F2
      case "f3" => ModifierKey.F3
      case "f4" => ModifierKey.F4
      case "f5" => ModifierKey.F5
      case "f6" => ModifierKey.F6
      case "f7" => ModifierKey.F7
      case "f8" => ModifierKey.F8
      case "f9" => ModifierKey.F9
      case "f10" => ModifierKey.F10
      case "f11" => ModifierKey.F11
      case "f12" => ModifierKey.F12
      case _ => null
    }
  }

  /*
  Para facilitar esta implementação segue alguns padrões:
  Os caracteres não especiais sempre no final concatenação
  Exemplo:
     shortcut('ctrl -> 'alt -> 'z') //Valido
     shortcut('ctrl -> 'z' -> 'alt) //Inválido

 */
  implicit class ImplicitSymbol(sym: Symbol) {
    def ->(otherSymbol: Symbol): Seq[Symbol] = {
      Seq(sym,otherSymbol)
    }
    def ->(char: Char): Seq[Key] = {
      val key = symbolToKey(sym)
      Seq(key,new CharKey(char))
    }
  }

  implicit class ImplicitSeqSymbol(syms: Seq[Symbol]) {
    def ->(char: Char): Seq[Key] = {
      val keys: Seq[Key] = SeqSymbolToSeqKey(syms)
      keys :+ new CharKey(char)
    }

    def ->(otherSymbol: Symbol): Seq[Symbol] = {
      syms :+ otherSymbol
    }
  }

  implicit def SeqSymbolToSeqKey(list: Seq[Symbol]): Seq[Key] = {
    list.map(s => symbolToKey(s))
  }
}
