package dsl.cap02.example5

import dsl.cap02.example1.model.{Group, Shortcut}
import dsl.cap02.example1.model.shortcut.CharKey
import dsl.cap02.example1.model.shortcut.ModifierKey._
import Dsl._

/**
 * Created by leonardootto on 01/09/2014.
 */
object Main {
  def main(args: Array[String]) {
    val groupFromDsl: Group = getGroupFromDsl
    val groupFromCQ: Group = getGroupFromCommandQuery
    System.out.println("Iguais? " + (groupFromCQ == groupFromDsl))
  }

  private def getGroupFromDsl: Group = {
    return group(
        shortcut('ctrl -> 'z'),
        shortcut('ctrl -> 'shift -> 'z'),
        shortcut('ctrl -> 'alt -> 'del),
        shortcut('alt -> 'f4)
    )
  }

  private def getGroupFromCommandQuery: Group = {
    val g: Group = new Group
    var s: Shortcut = new Shortcut
    s.addKey(Ctrl)
    s.addKey(new CharKey('z'))
    g.addShortcut(s)
    s = new Shortcut
    s.addKey(Ctrl)
    s.addKey(Shift)
    s.addKey(new CharKey('z'))
    g.addShortcut(s)
    s = new Shortcut
    s.addKey(Ctrl)
    s.addKey(Alt)
    s.addKey(Del)
    g.addShortcut(s)
    s = new Shortcut
    s.addKey(Alt)
    s.addKey(F4)
    g.addShortcut(s)
    return g
  }
}


