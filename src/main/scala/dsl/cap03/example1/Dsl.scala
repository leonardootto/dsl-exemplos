package dsl.cap03.example1

import dsl.cap03.example1.GameState._

/**
 * Created by leonardootto on 29/11/14.
 */
/**
 * Created by leonardootto on 17/11/2014.
 *
 */
object Dsl {

  implicit def dsl_to_rule(dsl: Dsl): Rule = {
    dsl.build()
  }

  def Regras(rules: Rule*) = rules.toList

  def qualquer: Qualquer = new Dsl()

}

trait Ou {
  def com(n: Int): ComNumero
}

trait ComNumero {
  def ou(): Ou

  def com(): Com

  def vizinhos: Vizinhos
}

trait Vizinhos {
  self: Dsl =>

  def morre = self

  def vive = self
}

trait De {


  def vizinhos: Vizinhos
}

trait Mais {
  def de(n: Int): De
}

trait Menos {
  def de(n: Int): De
}

trait Com {
  def mais: Mais

  def menos: Menos

  def exatamente(n: Int): De
}

trait CelulaViva {
  def com(n: Int): ComNumero

  def com: Com
}

trait CelulaMorta {
  def com: Com
}

trait Celula {
  def viva: CelulaViva

  def morta: CelulaMorta
}

trait Qualquer {
  def celula: Celula
}


class Dsl extends Qualquer
with Celula with CelulaViva with CelulaMorta
with Com with Mais with Menos with De with Vizinhos with ComNumero with Ou {

  var fromType: CellType = null
  var toType: CellType = null
  var predicateType: Class[_ <: Predicate] = null
  var predicate: Predicate = null
  var listOfPredicates:List[Predicate] = List()
  var orPredicate = false

  override def celula: Celula = {
    this
  }

  override def viva: CelulaViva = {
    fromType = LiveCell
    this
  }

  override def morta: CelulaMorta = {
    fromType = DeadCell
    this
  }

  override def com(n: Int): ComNumero = {
    predicateType = classOf[Equal]

    predicate = newPredicate(predicateType,n)
    listOfPredicates = listOfPredicates :+ predicate

    if(orPredicate){
      require(listOfPredicates.length >= 2)

      val twoPredicates = listOfPredicates.takeRight(2)
      listOfPredicates = listOfPredicates.dropRight(2) :+ Or(twoPredicates(0),twoPredicates(1))
    }

    this
  }

  override def com: Com = {
    this
  }

  override def menos: Menos = {
    predicateType = classOf[Less]
    this
  }

  override def exatamente(n: Int): De = {
    predicateType = classOf[Equal]

    predicate = newPredicate(predicateType,n)
    listOfPredicates = listOfPredicates :+ predicate
    this
  }

  override def mais: Mais = {
    predicateType = classOf[Greater]
    this
  }

  override def morre: Dsl = {
    toType = DeadCell
    this
  }

  override def vive: Dsl = {
    toType = LiveCell
    this
  }

  override def ou(): Ou = {
    // transform next and previous in one Or predicate
    orPredicate = true
    this
  }


  override def vizinhos: Dsl = {
    this
  }

  override def de(n: Int): De = {
    predicate = newPredicate(predicateType,n)
    listOfPredicates = listOfPredicates :+ predicate
    this
  }

  def newPredicate(predicate:Class[_<:Predicate], number:Int): Predicate ={
    predicate match {
      case r if r == classOf[Less] => Less(number, LiveCell)
      case r if r == classOf[Greater] => Greater(number, LiveCell)
      case r if r == classOf[Equal] => Equal(number, LiveCell)
    }
  }

  def build(): Rule = {
    Rule(fromType,listOfPredicates,toType)
  }
}


