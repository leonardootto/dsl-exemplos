package dsl.cap03.example1

import java.util.{Timer, TimerTask}

import dsl.cap03.example1.GameState._

object GameState {

  trait CellType

  object DeadCell extends CellType {
    override def toString: String = "D"
  }

  object LiveCell extends CellType {
    override def toString: String = "L"
  }

  type Row[T] = Array[T]
  type Matrix[T] = Array[Row[T]]

  val worldSize = 64

  def start = empty(worldSize)

  def startRandom = createRandom(worldSize)

  def startCustom = createCustom(worldSize)

  def empty(size: Int): GameState = {
    val array = Array.fill(size)(
      Array.fill[CellType](size)(DeadCell)
    )
    GameState(array)
  }

  val probabilityOfLiveCell = 0.3

  def createRandom(size: Int) = {
    val state = empty(size).state.map(_.map(_ =>
      Math.random() match {
        case d if d <= probabilityOfLiveCell => LiveCell
        case _ => DeadCell
      }
    ))
    GameState(state)
  }

  def createCustom(size: Int) = {
    val state = empty(size).state
    val customState = empty(size).state.zipWithIndex.map {
      case (row, i) => row.zipWithIndex.map {
        case (elem, j) => {
          val list = List(
            (0, 0),
            (0, 1),
            (1, 0),
            (1, 1)
          )
          if (list.contains((i, j))) {
            LiveCell
          } else {
            DeadCell
          }
        }
      }
    }

    GameState(customState)
  }

  trait Predicate {
    def apply(types: Seq[CellType]): Boolean
  }

  case class Or(first: Predicate, second: Predicate) extends Predicate {
    def apply(types: Seq[CellType]): Boolean = {
      first.apply(types) || second.apply(types)
    }
  }

  case class Equal(number: Int, cellType: CellType) extends Predicate {
    def apply(types: Seq[CellType]): Boolean = {
      number == types.count(_ == cellType)
    }
  }

  case class Greater(number: Int, cellType: CellType) extends Predicate {
    def apply(types: Seq[CellType]): Boolean = {
      number < types.count(_ == cellType)
    }
  }

  case class Less(number: Int, cellType: CellType) extends Predicate {
    def apply(types: Seq[CellType]): Boolean = {
      number > types.count(_ == cellType)
    }
  }

  case class Rule(initialCellState: CellType, predicates: List[Predicate], endCellState: CellType) {
    def run(actualCell: CellType, neighbourdHoods: Seq[CellType]): CellType = {
      if (actualCell != initialCellState) {
        actualCell
      } else {
        if (applicable(actualCell, neighbourdHoods)) {
          endCellState
        } else {
          actualCell
        }
      }
    }

    def applicable(actualCell: CellType, neighbourdHoods: Seq[CellType]): Boolean = {
      predicates.forall(p => p.apply(neighbourdHoods))
    }

  }

}

case class GameState(state: Matrix[CellType]) {
  /*
   * Qualquer célula viva com menos de dois vizinhos vivos morre de solidão.
   * Qualquer célula viva com mais de três vizinhos vivos morre de superpopulação.
   * Qualquer célula morta com exatamente três vizinhos vivos se torna uma célula viva.
   * Qualquer célula viva com dois ou três vizinhos vivos continua no mesmo estado para a próxima geração.
   */

  import dsl.cap03.example1.Dsl._

  val rules = Regras(
    qualquer.celula.viva.com.menos.de(2).vizinhos.morre,
    qualquer.celula.viva.com.mais.de(3).vizinhos.morre,
    qualquer.celula.morta.com.exatamente(3).vizinhos.vive,
    qualquer.celula.viva.com(2).ou().com(3).vizinhos.vive
  )
  //  val rules = List(
  //    Rule(LiveCell, List(Less(2, LiveCell)), DeadCell)
  //    Rule(LiveCell, List(Greater(3, LiveCell)), DeadCell),
  //    Rule(DeadCell, List(Equal(3, LiveCell)), LiveCell),
  //    Rule(LiveCell, List(Or(Equal(2, LiveCell), Equal(3, LiveCell))), LiveCell)
  //  )

  def iterate(): GameState = {
    val neighbourhoods = listNeighbourhoods()
    val maped = state.zipWithIndex.map {
      case (row, i: Int) => row.zipWithIndex.map {
        case (elem, j) => {

          val elemPositions = neighbourhoods.find(p => p._1 ==(i, j)).get._2
          val elems = elemPositions.map(seq => {
            state(seq._1)(seq._2)
          })

          val find = rules.find(r => r.applicable(elem, elems))
          if (find.isDefined) find.get.run(elem, elems)
          else elem

        }
      }
    }
    GameState(maped)
  }

  def listNeighbourhoods() = {

    //create list of indexes Neighbourhoods
    def getPositionList(size: Int) = {
      for {
        i <- 0 until size
        j <- 0 until size
      } yield (i, j)
    }
    val values = getPositionList(state.length)

    def getNeighbourhoosFromPositions(values: IndexedSeq[(Int, Int)], size: Int) = {
      val heigh = for {
        (i, j) <- values
      } yield {
        val tuples: Seq[(Int, Int)] = Seq((i, j - 1), (i - 1, j - 1), (i - 1, j),
          (i - 1, j + 1), (i, j + 1), (i + 1, j + 1),
          (i + 1, j), (i + 1, j - 1))

        // como se o mundo fosse redondo
        val validTuples: Seq[(Int, Int)] = tuples.map(p => {

          def correctOverflow(number: Int): Int = {
            if (number < 0) {
              size + number
            } else if (number >= size) {
              number - size
            } else {
              number
            }
          }

          (correctOverflow(p._1), correctOverflow((p._2)))
        })

        val tuple: ((Int, Int), Seq[(Int, Int)]) = ((i, j), validTuples)
        tuple
      }

      heigh
    }
    val neighbourhoods = getNeighbourhoosFromPositions(values, state.length)

    neighbourhoods
  }

  override def toString: String = {
    val line = Array.fill(state.length * 2)("-")
    val elements = state.map(row => {
      row.map(element => {
        ("|", element match {
          case LiveCell => "X"
          case _ => " "
        })
      }).flatMap(f => List(f._1, f._2))
    })

    val result = line +: elements :+ line
    val toString = result.foldLeft("")((result, array) => {
      val ret = array.foldLeft("")(
        (ret, element) => ret + element
      ) + "\n"
      result + ret
    })
    toString
  }
}


case class GameOfLife() {
  def timerCall(unit: => Unit): Unit = {
    new Timer().scheduleAtFixedRate(new TimerTask {
      override def run(): Unit = {
        unit
      }
    }, 0, 500)
  }


  def start(): Unit = {
    println("Wellcome to simple game of life")
    println("-------------------------------")
    println("create a random state")

    var game = GameState.startRandom //sorry for the var's
    var count = 0
    println(game)
    timerCall {
      game = game.iterate()
      count += 1
      println(s"--Iteration:$count-------------------------")
      println(game)
    }
  }
}

object Main {
  def main(args: Array[String]) {
    GameOfLife().start()
  }
}