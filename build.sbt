name := "Examples"

version := "1.0"

scalaVersion := "2.11.2"

libraryDependencies ++= Seq(
  "io.codearte.jfairy" % "jfairy" % "0.5.3",
  "org.apache.commons" % "commons-math3" % "3.6.1",
  "com.thoughtworks.xstream" % "xstream" % "1.4.7",
  "junit" % "junit" % "4.12",
  "org.scalatest" % "scalatest_2.11" % "2.2.6" % "test"
)
